# translation of plasma_applet_devicenotifier.po to Bengali INDIA
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Runa Bhattacharjee <runab@fedoraproject.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2008-07-04 12:04+0530\n"
"Last-Translator: Runa Bhattacharjee <runab@fedoraproject.org>\n"
"Language-Team: Bengali INDIA <discuss@ankur.org.in>\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:44
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Click to safely remove all devices"
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr ""

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr ""

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr ""

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr ""

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr ""

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr ""

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr ""

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr ""

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr ""

#: plugin/ksolidnotify.cpp:198
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "You are not authorized to mount this device."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:201
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:204
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "You are not authorized to eject this disc."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:211
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Could not mount this device as it is busy."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:242
#, fuzzy, kde-format
#| msgid ""
#| "Cannot eject the disc.\n"
#| "One or more files on this disc are open within an application."
msgid "One or more files on this device are open within an application."
msgstr ""
"ডিস্ক বের করা যায়নি।\n"
"কোনো অ্যাপ্লিকেশনের মধ্যে এই ডিস্কের এক অথবা একাধিক ফাইল খোলা রয়েছে।"

#: plugin/ksolidnotify.cpp:244
#, fuzzy, kde-format
#| msgid ""
#| "Cannot unmount the device.\n"
#| "One or more files on this device are open within an application."
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"ডিভাইস আন-মাউন্ট করা যায়নি।\n"
"কোনো অ্যাপ্লিকেশনের মধ্যে এই ডিভাইসের এক অথবা একাধিক ফাইল খোলা রয়েছে।"
msgstr[1] ""
"ডিভাইস আন-মাউন্ট করা যায়নি।\n"
"কোনো অ্যাপ্লিকেশনের মধ্যে এই ডিভাইসের এক অথবা একাধিক ফাইল খোলা রয়েছে।"

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ""

#: plugin/ksolidnotify.cpp:265
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Could not mount this device."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:268
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#: plugin/ksolidnotify.cpp:271
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Could not eject this disc."
msgstr "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"
#~ msgstr[1] "চিহ্নিত ডিভাইসের জন্য ১-টি কর্ম"

#, fuzzy
#~| msgid "<font color=\"%1\">Devices recently plugged in:</font>"
#~ msgid "Devices recently plugged in:"
#~ msgstr "<font color=\"%1\">সম্প্রতি সংযুক্ত করা ডিভাইস:</font>"

#, fuzzy
#~| msgid " sec"
#~ msgid " second"
#~ msgid_plural " seconds"
#~ msgstr[0] " sec"
#~ msgstr[1] " sec"

#~ msgid "&Time to stay on top:"
#~ msgstr "চিহ্নিত সময় অবধি উপরে স্থাপিত হবে: (&T)"

#~ msgid "&Number of items displayed:"
#~ msgstr "প্রদর্শিত বস্তুর সংখ্যা: (&N)"

#~ msgid "Unlimited"
#~ msgstr "সীমাবিহীন"

#~ msgid "&Display time of items:"
#~ msgstr "বস্তু সংক্রান্ত সময় প্রদর্শন করা হবে: (&D)"
