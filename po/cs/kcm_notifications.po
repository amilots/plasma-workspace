# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# SPDX-FileCopyrightText: 2019, 2020, 2021, 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-22 00:39+0000\n"
"PO-Revision-Date: 2023-11-02 13:53+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: kcm.cpp:70
#, kde-format
msgid "Toggle do not disturb"
msgstr "Zapnout na Nerušit"

#: sourcesmodel.cpp:392
#, kde-format
msgid "Other Applications"
msgstr "Ostatní aplikace"

#: ui/ApplicationConfiguration.qml:92
#, kde-format
msgid "Show popups"
msgstr "Zobrazit vyskakovací okna"

#: ui/ApplicationConfiguration.qml:106
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Zobrazit v režimu \"Nerušit\""

#: ui/ApplicationConfiguration.qml:119 ui/main.qml:183
#, kde-format
msgid "Show in history"
msgstr "Zobrazit v historii"

#: ui/ApplicationConfiguration.qml:130
#, kde-format
msgid "Show notification badges"
msgstr "Zobrazit počet upozornění"

#: ui/ApplicationConfiguration.qml:167
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Nastavit události"

#: ui/ApplicationConfiguration.qml:175
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr "Tato aplikace nepodporuje nastavení upozornění podle událostí"

#: ui/ApplicationConfiguration.qml:269
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Zobrazit zprávu ve vyskakovacím okně"

#: ui/ApplicationConfiguration.qml:278
#, kde-format
msgid "Play a sound"
msgstr "Přehrát zvuk"

#: ui/ApplicationConfiguration.qml:289
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview sound"
msgstr ""

#: ui/ApplicationConfiguration.qml:315
#, kde-format
msgctxt "Reset the notification sound to a default one"
msgid "Reset"
msgstr ""

#: ui/ApplicationConfiguration.qml:327
#, kde-format
msgid "Choose sound file"
msgstr ""

#: ui/main.qml:46
#, kde-format
msgctxt "@action:button Plasma-specific notifications"
msgid "System Notifications…"
msgstr ""

#: ui/main.qml:52
#, kde-format
msgctxt "@action:button Application-specific notifications"
msgid "Application Settings…"
msgstr ""

#: ui/main.qml:77
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Nebylo možné nalézt widget 'Upozornění', který je vyžadován pro zobrazování "
"upozornění. Ujistěte se, že je povolen buď v systémovém panelu nebo jako "
"samostatný widget."

#: ui/main.qml:88
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "Upozornění jsou aktuálně poskytována '%1 %2', namísto Plasma."

#: ui/main.qml:92
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Upozornění Plasma aktuálně neposkytuje."

#: ui/main.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Režim Nerušit"

#: ui/main.qml:104
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "Enable automatically:"
msgstr ""

#: ui/main.qml:105
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr ""

#: ui/main.qml:117
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr ""

#: ui/main.qml:132
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Manually toggle with shortcut:"
msgstr ""

#: ui/main.qml:139
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Podmínky viditelnosti"

#: ui/main.qml:144
#, kde-format
msgid "Critical notifications:"
msgstr "Kritické upozornění:"

#: ui/main.qml:145
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Zobrazit v režimu \"Nerušit\""

#: ui/main.qml:157
#, kde-format
msgid "Normal notifications:"
msgstr "Normální upozornění:"

#: ui/main.qml:158
#, kde-format
msgid "Show over full screen windows"
msgstr "Zobrazit přes okna přes celou obrazovku"

#: ui/main.qml:170
#, kde-format
msgid "Low priority notifications:"
msgstr "Nedůležitá upozornění:"

#: ui/main.qml:171
#, kde-format
msgid "Show popup"
msgstr "Zobrazit vyskakovací okno"

#: ui/main.qml:200
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Vyskakovací okna"

#: ui/main.qml:206
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Umístění:"

#: ui/main.qml:207
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Poblíž ikony upozornění"

#: ui/main.qml:244
#, kde-format
msgid "Choose Custom Position…"
msgstr "Vybrat vlastní pozici…"

#: ui/main.qml:253 ui/main.qml:269
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekunda"
msgstr[1] "%1 sekundě"
msgstr[2] "%1 sekundách"

#: ui/main.qml:258
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Skrýt po:"

#: ui/main.qml:281
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Další zpětná vazba"

#: ui/main.qml:287
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Zobrazit v upozorněních"

#: ui/main.qml:288
#, kde-format
msgid "Application progress:"
msgstr "Průběh aplikace:"

#: ui/main.qml:302
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Nezavírat vyskakovací okno před dokončením"

#: ui/main.qml:315
#, kde-format
msgid "Notification badges:"
msgstr "Počet upozornění:"

#: ui/main.qml:316
#, kde-format
msgid "Show in task manager"
msgstr "Zobrazit ve správci úloh"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Umístění vyskakovacího okna"

#: ui/SourcesPage.qml:20
#, kde-format
msgid "Application Settings"
msgstr "Nastavení aplikace"

#: ui/SourcesPage.qml:107
#, kde-format
msgid "Applications"
msgstr "Aplikace"

#: ui/SourcesPage.qml:108
#, kde-format
msgid "System Services"
msgstr "Systémové služby"

#: ui/SourcesPage.qml:156
#, kde-format
msgid "No application or event matches your search term"
msgstr "Vyhledávanému termínu neodpovídá žádná aplikace ani událost"

#: ui/SourcesPage.qml:180
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr "Vyberte v seznamu aplikaci, jejíž upozornění chcete nastavit"
